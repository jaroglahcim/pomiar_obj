#ifdef _WIN32
    #include "serialport_win.h"
    #include <iostream>

    SerialPortWin::SerialPortWin(std::string fileName) : SerialPort{fileName}
    {
        if (!port_.empty())
        {
            open();
        }
    }

    SerialPortWin::~SerialPortWin()
    {
        close();
    }

    void SerialPortWin::open()
    {
        DEBUG_LOG("Otwieranie portu szeregowego\n");
        if (!isOpen_)
        {
            serial_port_ = CreateFileA( ("\\\\.\\" + port_).c_str(),  // port name
                                        GENERIC_READ | GENERIC_WRITE, // Read/Write
                                        0,                            // No Sharing
                                        NULL,                         // No Security
                                        OPEN_EXISTING,                // Open existing port only
                                        0,                            // Non Overlapped I/O
                                        NULL);                        // Null for Comm Devices
            if (serial_port_ == INVALID_HANDLE_VALUE)
            {
                DEBUG_LOG("Błąd otwarcia portu szeregowego\n");
            }
            else
            {
                configure();
            }
        }
    }

    void SerialPortWin::close()
    {
        if (isOpen_)
        {
            DEBUG_LOG("Zamykanie portu szeregowego\n");
            CloseHandle(serial_port_);
            isOpen_ = false;
        }
    }

    void SerialPortWin::configure()
    {
        DEBUG_LOG("Konfiguracja portu szeregowego\n");
        DCB dcb;
        FillMemory(&dcb, sizeof(dcb), 0);
        if (!GetCommState(serial_port_, &dcb))
        {
            DEBUG_LOG("Błąd w GetCommStare\n");
            return;
        }

        dcb.DCBlength = sizeof(dcb);
        dcb.BaudRate = CBR_9600;
        dcb.ByteSize = 8;
        dcb.StopBits = ONESTOPBIT;
        dcb.Parity = NOPARITY;

        if (!SetCommState(serial_port_, &dcb))
        {
            DEBUG_LOG("Błąd w GetCommStare\n");
            return;
        }

        COMMTIMEOUTS timeouts;
        timeouts.ReadIntervalTimeout = 100;
        timeouts.ReadTotalTimeoutConstant = 100;
        timeouts.ReadTotalTimeoutMultiplier = 2;
        timeouts.WriteTotalTimeoutConstant = 500;
        timeouts.WriteTotalTimeoutMultiplier = 4;

        if (!SetCommTimeouts(serial_port_, &timeouts))
        {
            DEBUG_LOG("Błąd w SetCommTimeouts\n");
            return;
        }

        isOpen_ = true;
    }

    ssize_t SerialPortWin::read(char* buffer, int buff_size)
    {
        DWORD n_read = 0;
        int total_read = 0;
        if (isOpen_)
        {
            char buf;
            do
            {
                ReadFile(serial_port_, &buf, sizeof(buf), &n_read, nullptr);
                if (n_read > 0)
                {
                    buffer[total_read] = buf;
                    total_read++;
                }
            }
            while(buf != '\r' && n_read > 0 && total_read < buff_size);

        }
        return (ssize_t) total_read;
    }

    ssize_t SerialPortWin::write(const std::string& message)
    {
        DWORD n_written = 0;
        if (isOpen_)
        {
            bool if_written = WriteFile(serial_port_, message.c_str(), (DWORD) message.size(), &n_written, nullptr);
            if (!if_written)
            {
                DEBUG_LOG("Nie udało się wysłać wiadomości: %s\t\tRozmiar wiadomości: %d\n", message.c_str(), (int) message.size());
            }
        }
        return (ssize_t) n_written;
    }
#endif
