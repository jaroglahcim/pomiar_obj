// #include "serialport.h"
#ifdef __linux__
    #include "serialport_linux.h"
    #include <cerrno> // Error integer
    #include <cstring> // strerror() function
    #include <fcntl.h> // open(), O_RDWR
    #include <iostream>
    #include <termios.h>
    #include <unistd.h> // write(), read(), close()

    SerialPortLinux::SerialPortLinux(std::string fileName) : SerialPort{fileName}
    {
        if (!port_.empty())
        {
            open();
        }
    }

    SerialPortLinux::~SerialPortLinux()
    {
        close();
    }

    void SerialPortLinux::open()
    {
        DEBUG_LOG("Otwieranie portu szeregowego\n");
        if (!isOpen_)
        {
            serial_port_ = ::open(port_.c_str(), O_RDWR);
            if (serial_port_ < 0)
            {
                DEBUG_LOG("Błąd nr %d funkcji open: %s\n", errno, strerror(errno));
            }
            else
            {
                configure();
            }
        }
    }

    void SerialPortLinux::configure()
    {
        struct termios tty;

        if (tcgetattr(serial_port_, &tty) != 0)
        {
            DEBUG_LOG("Błąd nr %d funkcji tcgetattr: %s\n", errno, strerror(errno));
            return;
        }

        tty.c_cflag &= ~PARENB; // Clear parity bit, disabling parity (most common)
        // tty.c_cflag |= PARENB;  // Set parity bit, enabling parity
        tty.c_cflag &= ~CSTOPB; // Clear stop field, only one stop bit used in communication (most common)
        // tty.c_cflag |= CSTOPB;  // Set stop field, two stop bits used in communication
        tty.c_cflag &= ~CSIZE; // Clear all the size bits, then use one of the statements below
        // tty.c_cflag |= CS5; // 5 bits per byte
        // tty.c_cflag |= CS6; // 6 bits per byte
        // tty.c_cflag |= CS7; // 7 bits per byte
        tty.c_cflag |= CS8; // 8 bits per byte (most common)
        tty.c_cflag &= ~CRTSCTS; // Disable RTS/CTS hardware flow control (most common)
        // tty.c_cflag |= CRTSCTS;  // Enable RTS/CTS hardware flow control
        tty.c_cflag |= CREAD | CLOCAL; // Turn on READ & ignore ctrl lines (CLOCAL = 1)
        
        tty.c_lflag &= ~ICANON;
        tty.c_lflag &= ~ECHO; // Disable echo
        tty.c_lflag &= ~ECHOE; // Disable erasure
        tty.c_lflag &= ~ECHONL; // Disable new-line echo
        tty.c_lflag &= ~ISIG; // Disable interpretation of INTR, QUIT and SUSP
        tty.c_iflag &= ~(IXON | IXOFF | IXANY); // Turn off s/w flow ctrl
        tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL); // Disable any special handling of received bytes
        
        tty.c_oflag &= ~OPOST; // Prevent special interpretation of output bytes (e.g. newline chars)
        tty.c_oflag &= ~ONLCR; // Prevent conversion of newline to carriage return/line feed
        
        tty.c_cc[VTIME] = 10;
        tty.c_cc[VMIN] = 0;
        
        // Set in/out baud rate to be 9600
        cfsetispeed(&tty, B9600);
        cfsetospeed(&tty, B9600);

        // Save tty settings, also checking for error
        if (tcsetattr(serial_port_, TCSANOW, &tty) != 0)
        {
            DEBUG_LOG("Błąd nr %d funkcji tcgetattr: %s\n", errno, strerror(errno));
            return;
        }
        
        isOpen_ = true;
    }

    void SerialPortLinux::close()
    {
        if (isOpen_)
        {
            ::close(serial_port_);
            isOpen_ = false;
        }
    }

    //buffer memory already allocated, overflow unsafe
    ssize_t SerialPortLinux::read(char* buffer, int buff_size)
    {
        ssize_t n_read = 0;
        char buf = '\0';
        ssize_t total_read = 0;
        
        if (isOpen_)
        {
            do
            {
                n_read = ::read(serial_port_, &buf, 1);
                if (n_read > 0)
                {
                    buffer[total_read] = buf;
                    total_read++;
                }
            }
            // while (buf != '\r' && n_read > 0);
            while (buf != '\r' && n_read > 0 && total_read < buff_size);
        }

        return total_read;
    }

    ssize_t SerialPortLinux::write(const std::string& message)
    {
        ssize_t n_written = 0;

        if (isOpen_)
        {
            n_written = ::write(serial_port_, message.c_str(), message.size());
            if (n_written < 0)
            {
                DEBUG_LOG("Nie udało się wysłać wiadomości: %s\n", message.c_str());
            }
        }

        return n_written;
    }
#endif
