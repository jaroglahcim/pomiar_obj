#include <string>
#define BUFF_SIZE 1024
// #define BUFF_SIZE 868

#ifndef SERIALPORT

// TODO: move debug defines to common if there will be something more to send there
// #define DEBUG
#ifdef DEBUG
    #define DEBUG_LOG(...) fprintf(stderr, __VA_ARGS__)
#else
    #define DEBUG_LOG(...) while(false) {}
#endif

#define SERIALPORT
class SerialPort
{
public:
    SerialPort(const std::string& pt) { port_ = pt; }
    virtual ~SerialPort() {}
    //open a serial port, store its number to port_
    virtual void open() = 0;
    //close a serial port
    virtual void close() = 0;
    //store data from an opened serial port to the buffer
    virtual ssize_t read(char* buff, int buff_size=BUFF_SIZE) = 0;
    //write message to an opened serial port
    virtual ssize_t write(const std::string& message) = 0;
    bool isOpen() { return isOpen_; }
protected:
    std::string port_ = "";
    bool isOpen_ = false;
    virtual void configure() = 0;
};

#endif
