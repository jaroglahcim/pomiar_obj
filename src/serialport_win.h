#ifdef _WIN32
    #include "serialport.h"
    #include <windows.h>
    class SerialPortWin : public SerialPort
    {
    public:
        SerialPortWin(std::string fileName);
        ~SerialPortWin();
        void open() override;
        void close() override;
        ssize_t read(char* buffer, int buff_size) override;
        ssize_t write(const std::string& message) override;
    private:
        HANDLE serial_port_;
        void configure() override;
    };
#endif
