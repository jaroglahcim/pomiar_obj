#include <string> //string
#include <vector> //vector
#include <ctime>  //time_t

#ifdef __linux__
    #define DEFAULT_DEVICE "/dev/ttyUSB0"
    #define PROGRAM_LAUNCH_NAME "pomiar_obj"
#elif _WIN32
    #define DEFAULT_DEVICE "COM3"
    #define PROGRAM_LAUNCH_NAME "pomiar_obj.exe"
#endif

class Configuration{
    public:
        Configuration(std::string device=DEFAULT_DEVICE, std::time_t interval = 5, unsigned int movmean_window = 1, std::string log_file = "pomiar.csv",
                        std::vector<std::string> parameters = std::vector<std::string>{"temperature"}, std::string config_file="", std::string msg_pass="1122\n");

        //get full configuration data
        friend std::ostream& operator<<(std::ostream &out, const Configuration& c);
        std::string to_string();

        //stored option variables
        std::string device;
        std::time_t interval;
        unsigned int movmean_window;
        std::string log_file;
        std::vector<std::string> parameters;
        std::string config_file;

        void get_configuration_from_file();

        //flag determining whether to terminate the program after configuration options were determined (example: display help screen only)
        bool if_terminate = false;
        //only read access to password
        std::string const get_password();
    private:
        //helper function used by get_configuration_from_file()
        void store_config_line(std::string key, std::string value);
        //private password, change forbidden outside class
        std::string msg_pass;
};

namespace pomiar_obj
{
    //helper hash function, used in Configuration::store_config_line() and parse_terminal_arguments()
    constexpr unsigned int hash(const char* str, int h = 0)
    {
        return !str[h] ? 5381 : (hash(str, h+1)*33) ^ str[h];
    }
}
