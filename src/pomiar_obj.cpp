#include "configuration.h"
#include "serialport.h"

// C++ library headers
#include <cstring> // memset()
#include <ctime> // time(), asctime()
#include <fstream>
#include <iostream>
#include <string> // pop_back()
#include <sstream> //stringstream
#include <iomanip> //quoted
#include <vector>
#include <map>
#include <queue>
#include <signal.h>

// Linux headers
#ifdef __linux__
    #include <sys/ioctl.h>
    #include <termios.h>
    #include <unistd.h>
    #include "serialport_linux.h"
// Windows headers
#elif _WIN32
    #include <windows.h>
    #include <conio.h>
    #include "serialport_win.h"
#endif

//check if button pressed by getting data from the terminal, linux only; windows uses conio.h functions
#ifdef __linux__
    // Turn off buffering input in terminal until ENTER for immediate detection
    void enable_raw_mode()
    {
        termios term;
        tcgetattr(0, &term);
        term.c_lflag &= ~(ICANON | ECHO); // Disable echo too
        tcsetattr(0, TCSANOW, &term);
    }

    void disable_raw_mode()
    {
        termios term;
        tcgetattr(0, &term);
        term.c_lflag |= ICANON | ECHO;  // Enable echo too
        tcsetattr(0, TCSANOW, &term);
    }

    int _kbhit()
    {
        static const int STDIN = 0;
        int bytesWaiting;
        ioctl(STDIN, FIONREAD, &bytesWaiting);
        return bytesWaiting;
    }
#endif

//termination signal handlers
#ifdef __linux__
    volatile sig_atomic_t signal_received = 0;

    void signal_handler(int s) {
        printf("Otrzymano polecenie zatrzymania programu");
        fflush(stdout);
        DEBUG_LOG(": %d", s);
        #ifndef DEBUG
            std::ignore = s;
        #endif
        printf("\n");
        signal_received = 1;
    }
#elif _WIN32
    volatile static int signal_received = 0;
    BOOL WINAPI CtrlHandler(DWORD fdwCtrlType)
    {
        switch (fdwCtrlType)
        {
            // Handle the CTRL-C signal.
            case CTRL_C_EVENT:
                printf("Otrzymano polecenie zatrzymania programu");
                DEBUG_LOG(" (Ctrl-C)");
                printf("\n");
                signal_received = 1;
                return TRUE;
            // CTRL-CLOSE: confirm that the user wants to exit.
            case CTRL_CLOSE_EVENT:
                printf("Otrzymano polecenie zatrzymania programu");
                DEBUG_LOG(" (Ctrl-Close)");
                printf("\n");
                signal_received = 1;
                return TRUE;
            default:
                return FALSE;
        }
    }

#endif

//look through the buffer to return a string of the first numerical value found after the specified pattern string
//returns empty string if no value found
std::string get_value_of(char* buff, size_t buff_size, std::string pattern, bool verbose=true)
{
    std::string buffer(buff);
    char const* digits = "0123456789.-";
    std::string value = "";

    size_t found = buffer.find(pattern);
    if (found != std::string::npos)
    {
        size_t n = buffer.find_first_of(digits, found);
        size_t m = buffer.find_first_not_of(digits, n);
        if (n > buff_size || m > buff_size)
        {
            DEBUG_LOG("Odczyt poza rozmiarem bufora\n");
            return value;
        }
        value = buffer.substr(n, m - n);
        if (verbose)
        {
            printf("%s %s\n", pattern.c_str(), value.c_str());
            // std::cout << pattern << " " << value << "\n" << std::flush;
        }
    }

    return value;
}

//look through the buffer to return strings of first numerical values found after every occurence of the specified pattern string
std::vector<std::string> get_values_of(char* buff, size_t buff_size, std::string pattern, bool verbose=true)
{
    std::string buffer(buff);
    char const* digits = "0123456789.-";
    std::vector<std::string> values;
    std::string value = "";

    size_t found = buffer.find(pattern);
    while (found != std::string::npos)
    {
        size_t n = buffer.find_first_of(digits, found);
        size_t m = buffer.find_first_not_of(digits, n);

        if (n > buff_size || m > buff_size)
        {
            DEBUG_LOG("Odczyt poza rozmiarem bufora\n");
            return values;
        }

        value = buffer.substr(n, m - n);
        values.push_back(value);
        if (verbose)
        {
            printf("%s %s\n", pattern.c_str(), value.c_str());
            // std::cout << pattern << " " << value << "\n";
        }

        found = buffer.find(pattern, m);
    }
    return values;
}

//sleep for x milliseconds, appropriate command for an OS
void sleep(int milliseconds)
{
    //give the device some time to receive commands properly <- on a windows virtual machine, it didn't parse the password properly at the start
    //during tests before adding it, for example
    #ifdef __linux__
        //linux sleep, in microseconds (10^-6)
        usleep(milliseconds * 1000);
    #elif _WIN32
        //windows sleep, in milliseconds (10^-3)
        Sleep(milliseconds);
    #endif
}
//helper function, check if next terminal argument is a proper value, used in parse_terminal_arguments()
bool if_next_argument_value_exists(int counter, int amount, std::vector<std::string> terminal_arguments, bool error_messages_show=true)
{
    //if there's no next terminal argument 
    if (counter >= amount - 1)
    {
        if (error_messages_show)
        {
            DEBUG_LOG("Nie podano wartości parametru %s. Wybrano domyślną\n", terminal_arguments[counter].c_str());
        }
        return false;
    }
    std::string s = terminal_arguments[counter + 1];
    //if next terminal argument is not a value but a different option
    if (s.c_str()[0] == '-' || s.c_str()[0] == '?')
    {
        if (error_messages_show)
        {
            DEBUG_LOG("Nie podano wartości parametru %s. Wybrano domyślną\n", terminal_arguments[counter].c_str());
        }
        return false;
    }
    return true;
}

//parses terminal arguments and returns Configuration object which holds all config data of the program
Configuration* parse_terminal_arguments(int argc, char **argv)
{
    //store all terminal arguments into a vector
    std::vector<std::string> terminal_arguments;
    for (int i = 1; i < argc; ++i)
    {
        terminal_arguments.push_back(argv[i]);
    }

    //message to display when help option chosen, TO DO store it in a better way
    std::string help =  "\n?, -h\t\t\tpomoc krótka"
                        "\n-help\t\t\tpomoc pełna, tu jesteś\n"
                        "-d, -device\t\tścieżka dostępu do urządzenia/głowicy\t\t\t\t\t\twartość domyślna: " DEFAULT_DEVICE "\n"
                        "-l, -logfile\t\tścieżka do pliku tekstowego, w którym pomiary zostaną zapisane\t\t\twartość domyślna: pomiar_obj.txt\n"
                        "-i, -interval\t\tczęstotliwość odczytów w sekundach\t\t\t\t\t\twartość domyślna: 5\n"
                        "-p, -parameters\t\tzestaw szukanych parametrów do pomiaru i zapisu\t\t\t\t\twartość domyślna: temperature\n"
                        "\t\t\tparametry ze spacją w nazwie należy podać w cudzysłowie\t\t\t\tPrzykładowe parametry: Vc, Vb, Vm, Vr, \"Reverse cyclic volumes to alarm\"\n\n"
                        "-m, -movmean\t\tliczba pomiarów do wyliczania średnich kroczących przyrostów\t\t\twartość domyślna: BRAK\n"
                        "\t\t\tjest wyznaczana tylko dla wartości typu Vx, których pomiary zostały określone jako parametry wywołania programu\n"
                        "\t\t\tnp. wartość movmean=3 oznacza trzy kolejne pomiary czyli wypisywanie średniej z dwóch ubiegłych przyrostów, razem z obecnym pomiarem\n\n"
                        "-c, -config\t\tścieżka względna do pliku konfiguracyjnego z ustawieniami wywołania programu\n"
                        "\t\t\tnadpisuje opcje z argumentów i/lub wartości domyślne\n"
                        "\t\t\tw każdej linii jedna opcja w formacie klucz=wartość, bez spacji wokół znaku \"=\"\n"
                        "\t\t\tw przypadku ciągu wartości, oddziela się je znakiem \",\", bez spacji przed i za przecinkiem (\"=\")\n"
                        "\t\t\tOpcje nieokreślone w pliku przyjmą wartość podaną w linii komend lub w przypadku ich braku, przyjmą wartość domyślną\n"
                        "BRAK:\t\t\twywołanie programu bez żadnego parametru inicjuje interaktywny wybór opcji wywołania programu; Enter zatwierdza wpisaną opcję\n"
                        "\t\t\tw przypadku braku wpisania zawartości program przyjmuje wartość domyślną, podaną w nawiasie\n\n\n"
                        "Przykładowa zawartość pliku konfiguracyjnego:\n\n"
                        "device=" DEFAULT_DEVICE "\ninterval=30\nparameters=Vc,battery 1,temperature,voltage\nlogfile=pomiar.csv\nmovmean=3\n"
#ifdef DEBUG
                        "password=1234\n"
#endif
                        "Przykładowe zapytanie:\t" PROGRAM_LAUNCH_NAME " -d " DEFAULT_DEVICE " -i 30 -p Vc \"battery 1\" temperature voltage -l pomiar.csv -m 3\n"
                        "\t\t\t" PROGRAM_LAUNCH_NAME " -c cfg.txt\n\n";

    std::string help_simple =   "\n?, -h\t\tpomoc krótka, tu jesteś\n"
                                "-help\t\tpomoc pełna\n"
                                "-d, -device\tścieżka do głowicy\n\t\tdomyślnie: " DEFAULT_DEVICE "\n"
                                "-l, -logfile\tścieżka do pliku logów\n\t\tdomyślnie: pomiar_obj.txt\n"
                                "-i, -interval\tczęstotliwość odczytów w sekundach\n\t\tdomyślnie: 5\n"
                                "-p, -parameters\tzestaw szukanych parametrów do pomiaru i zapisu\n\t\tdomyślnie: temperature\n"
                                "-m, -movmean\trozmiar okna dla średniej kroczącej z przyrostów parametrów\n"
                                "-c, -config\tścieżka względna do pliku konfiguracyjnego,\n\t\tnadpisuje opcje z argumentów i/lub wartości domyślne\n"
                                "BRAK\t\tinteraktywny wybór opcji\n\n";
    
    Configuration* configuration = new Configuration();
    std::string s;
    int counter = 0;
    int amount = (int) terminal_arguments.size();
    bool option_mode_set = false;

    //iterate over arguments and store data to proper elements of an object of Configuration class
    while (counter < amount)
    {
        s = terminal_arguments[counter];
        //simple hash function used for conversion of strings to a constexpr, to use a switch
        //alternative: a lot of if comparisons like if (s == "-h" || s == "?" || <...>)
        switch(pomiar_obj::hash(s.c_str()))
        {
            //display the help message, set if_terminate to stop running the program immediately afterwards
            case pomiar_obj::hash("?"): case pomiar_obj::hash("-h"):
                option_mode_set = true;
                std::cout << help_simple;
                configuration->if_terminate = true;
                return configuration;
            case pomiar_obj::hash("help"): case pomiar_obj::hash("-help"):
                option_mode_set = true;
                std::cout << help;
                configuration->if_terminate = true;
                return configuration;

            //set path to the config file, passed in the next terminal argument; if no argument value, skip it as if the option was not set at all
            case pomiar_obj::hash("-c"): case pomiar_obj::hash("-config"):
                option_mode_set = true;
                if (if_next_argument_value_exists(counter, amount, terminal_arguments))
                {
                    s = terminal_arguments[counter + 1];
                    configuration->config_file = s;
                    DEBUG_LOG("Określono plik konfiguracyjny: %s\n", s.c_str());
                    ++counter;
                }
                ++counter;
                break;

            
            //set path to the device, passed in the next terminal argument; if no argument value, skip it as if the option was not set at all
            case pomiar_obj::hash("-d"): case pomiar_obj::hash("-device"):
                option_mode_set = true;
                if (if_next_argument_value_exists(counter, amount, terminal_arguments))
                {
                    s = terminal_arguments[counter + 1];
                    configuration->device = s;
                    DEBUG_LOG("Określono urządzenie na porcie: %s\n", s.c_str());
                    ++counter;
                }
                ++counter;
                break;

            //set path to the log file, passed in the next terminal argument; if no argument value, skip it as if the option was not set at all
            case pomiar_obj::hash("-l"): case pomiar_obj::hash("-log"): case pomiar_obj::hash("-logfile"):
                option_mode_set = true;
                if (if_next_argument_value_exists(counter, amount, terminal_arguments))
                {
                    s = terminal_arguments[counter + 1];
                    configuration->log_file = s;
                    DEBUG_LOG("Określono plik logów: %s\n", s.c_str());
                    ++counter;
                }
                ++counter;
                break;

            //set time between intervals, passed in the next terminal argument; if no proper argument value, skip it as if the option was not set at all
            case pomiar_obj::hash("-i"): case pomiar_obj::hash("-interval"):
            {
                option_mode_set = true;
                char *conversion_result;
                if (if_next_argument_value_exists(counter, amount, terminal_arguments))
                {
                    s = terminal_arguments[counter + 1];

                    //convert string to a float, check if valid number - strtod sets value pointed to by conversion_result to '\0' if conversion was valid
                    double d = strtod(s.c_str(), &conversion_result);
                    if (*conversion_result == '\0')
                    {
                        configuration->interval = (time_t) d;
                        DEBUG_LOG("Określono interwał odczytów: %.2fs\n", d);
                        ++counter;
                    }
                }
                ++counter;
                break;
            }
            //store all parameters to be measured; if no proper argument value, skip it as if the option was not set at all
            case pomiar_obj::hash("-p"): case pomiar_obj::hash("-parameters"):
            {
                option_mode_set = true;
                bool firstLoop = true;
                while (if_next_argument_value_exists(counter, amount, terminal_arguments, firstLoop))
                {
                    //reset parameters vector to remove default values
                    if (firstLoop)
                    {
                        configuration->parameters.clear();
                    }
                    s = terminal_arguments[counter + 1];
                    configuration->parameters.push_back(s);
                    DEBUG_LOG("Określono parametr do odczytywania: %s\n", s.c_str());
                    ++counter;
                    firstLoop = false;
                }
                ++counter;
                break;
            }
            //store all parameters to be measured; if no proper argument value, skip it as if the option was not set at all
            case pomiar_obj::hash("-m"): case pomiar_obj::hash("-movmean"):
            {
                option_mode_set = true;
                char *conversion_result;
                if (if_next_argument_value_exists(counter, amount, terminal_arguments))
                {
                    s = terminal_arguments[counter + 1];

                    //convert string to long, check if valid number - strtol sets value pointed to by conversion_result to '\0' if conversion was valid
                    long i = strtol(s.c_str(), &conversion_result, 10);
                    if (*conversion_result == '\0')
                    {
                        configuration->movmean_window = (unsigned int) i;
                        DEBUG_LOG("Określono okno dla średniej kroczącej: %ds\n", configuration->movmean_window);
                        ++counter;
                    }
                }
                ++counter;
                break;
            }
        }
    }

    //INTERACTIVE OPTIONS
    if (!option_mode_set)
    {
        std::cout << "Podaj ścieżkę do portu optycznego, do którego przyłączony jest gazomierz (domyślnie: " << configuration->device << "): ";
        getline(std::cin, s);
        DEBUG_LOG("Ścieżka do portu: %s", s.c_str());
        if (s != "")
        {
            configuration->device = s;
        }

        std::cout << "Podaj ciąg parametrów do odczytywania, rozdzielonych spacją (domyślnie: " << configuration->parameters[0] << "): ";
        getline(std::cin, s);

        DEBUG_LOG("Parametry: ");
        std::istringstream row(s);
        std::string param;
        bool first_param_set_flag = false;
        while (row >> std::quoted(param))
        {
            if (!first_param_set_flag)
            {
                configuration->parameters.clear();
            }
            DEBUG_LOG("\"%s\" ", param.c_str());
            configuration->parameters.push_back(param);
            first_param_set_flag = true;
        }
        DEBUG_LOG("\n");

        std::cout << "Podaj częstotliwość odczytów [s] (domyślnie: " << configuration->interval << "): ";
        getline(std::cin, s);
        char *conversion_result;
        if (s != "")
        {
            double d = strtod(s.c_str(), &conversion_result);
            if (*conversion_result == '\0')
            {
                configuration->interval = (time_t) d;
            }
            else
            {
                std::cout << "Wartość " << s << " jest nieprawidłowa. Pozostawiono domyślne ustawienia\n";
            }
        }

        std::cout << "Podaj liczbę ostatnich odczytów objętości, uwzględnianych w średniej (domyślnie: BRAK): ";
        getline(std::cin, s);
        if (s != "")
        {
            long l = strtol(s.c_str(), &conversion_result, 10);
            if (*conversion_result == '\0')
            {
                configuration->movmean_window = (int) l;
            }
            else
            {
                std::cout << "Wartość " << s << " jest nieprawidłowa. Pozostawiono domyślne ustawienia\n";
            }
        }

        std::cout << "Podaj ścieżkę do wyjściowego pliku logów dokonanych pomiarów (domyślnie: " << configuration->log_file << "): ";
        getline(std::cin, s);
        if (s != "")
        {
            configuration->log_file = s;
        }

        std::cout << "Upewnij się, że głowica jest podłączona a gazomierz wybudzony, by uzyskać pomiary od razu po uruchomieniu programu";
        std::getchar();
    }


    if (configuration->config_file != "")
    {
        configuration->get_configuration_from_file();
    }

    if (signal_received)
    {
        configuration->if_terminate = true;
    }

    // manually change unsupported configurations options to supported ones
    if (configuration->interval < 2)
    {
        configuration->interval = 2;
    }
    return configuration;
}

//helper function, add message_out to output file ofstream only if bool to_header is true and always add message_buffer to stringstream buffer 
void add_to_streams(std::string message_header, std::string message_measurement, std::stringstream* header, std::stringstream* measurement, bool to_header)
{
    if (to_header)
    {
        *header << message_header;
    }
    *measurement << message_measurement;
}

bool if_all_params(std::string header, Configuration configuration)
{
    size_t found = header.find("Date");
    if (found == std::string::npos)
    {
        return false;
    }
    for (size_t i = 0; i < configuration.parameters.size(); ++i)
    {
        found = header.find(configuration.parameters[i]);
        if (found == std::string::npos)
        {
            return false;
        }
    }
    return true;
}

struct log_streams {
    std::stringstream reading, header;
    std::map<std::string, std::queue<std::string>> measurement_values;
    bool firstLoop;
};

//look through the buffer to get values of requested parameters and append the log file with them
//stores values to a stringstream buffers to wait till a successful access with all parameters for a proper header
bool get_readings(Configuration configuration, log_streams* s, char* read_buf, size_t buff_size, std::ofstream* output_file, std::string read_time_str)
{
    std::string comma = ", ";
    int amount = (int) configuration.parameters.size();
    s->header.clear();
    s->header.str("");

    //omit garbage from previous messages
    std::string beginning = "Measurement data:";
    std::string buffer(read_buf);
    size_t found = buffer.find(beginning);
    if (found == std::string::npos)
    {
        for (int i = 0; i < amount; ++i)
        {
            if (s->measurement_values[configuration.parameters[i]].size() > 0)
            {
                std::string value_to_replicate = s->measurement_values[configuration.parameters[i]].back();
                s->measurement_values[configuration.parameters[i]].push(value_to_replicate);
                DEBUG_LOG("Zapisywanie parametru %s o wartości %s do okna dla średniej kroczącej\n", configuration.parameters[i].c_str(), value_to_replicate.c_str());
                if (s->measurement_values[configuration.parameters[i]].size() > configuration.movmean_window)
                {
                    DEBUG_LOG("Usuwanie parametru %s o wartości %s z okna dla średniej kroczącej\n", configuration.parameters[i].c_str(), s->measurement_values[configuration.parameters[i]].front().c_str());
                    s->measurement_values[configuration.parameters[i]].pop();
                }
            }
        }
        return false;
    }
    read_buf = read_buf + found + beginning.size();

    add_to_streams("Date, ", read_time_str + comma, &(s->header), &(s->reading), s->firstLoop);
    // reading << read_time_str << comma;
    for (int i = 0; i < amount; ++i)
    {
        //get all values with same label: get_values_of displays values on screen already, formatting needed for the log file,
        //store readings for moving average calculations

        std::vector<std::string> values = get_values_of(read_buf, buff_size, configuration.parameters[i]);
        for (size_t j = 0; j < values.size(); j++)
        {
            //store measurement values for movmean window - add latest measurement, remove element which is leaving the mean window
            // DEBUG_LOG("Zapisywanie parametru %s o wartości %s do okna dla średniej kroczącej\n", configuration.parameters[i].c_str(), values[j].c_str());
            s->measurement_values[configuration.parameters[i]].push(values[j]);
            if (s->measurement_values[configuration.parameters[i]].size() > configuration.movmean_window)
            {
                // DEBUG_LOG("Usuwanie parametru %s o wartości %s z okna dla średniej kroczącej\n", configuration.parameters[i].c_str(), s->measurement_values[configuration.parameters[i]].front().c_str());
                s->measurement_values[configuration.parameters[i]].pop();
            }
            add_to_streams(configuration.parameters[i], values[j], &(s->header), &(s->reading), s->firstLoop);
            if (j < values.size() - 1)
            {
                add_to_streams(comma, comma, &(s->header), &(s->reading), s->firstLoop);
            }    
        }

        if (i < amount - 1)
        {
            add_to_streams(comma, comma, &(s->header), &(s->reading), s->firstLoop);
        }
    }
    add_to_streams("\n", "\n", &(s->header), &(s->reading), s->firstLoop);

    // cerr << (read_time_str + comma).length() << "\n" << std::flush;
    if (s->reading.str().length() <= (read_time_str + comma + " ").length())
    {
        return false;
    }

    if (!s->firstLoop)
    {
        //unload readout stringstream to output file
        // cerr << "Length: " << s->reading.str().length();
        *output_file << s->reading.str() << std::flush;
        s->reading.clear();
        s->reading.str("");
    }

    else if (if_all_params(s->header.str(), configuration))
    {
        //unload readout header + all previous readouts to the output file
        *output_file << s->header.str() << s->reading.str() << std::flush;
        s->firstLoop = false;
        s->reading.clear();
        s->reading.str("");
        s->header.clear();
        s->header.str("");
    }

    else
    {
        return false;
    }
    return true;
}

//write a static header with the list of arguments to output_file
void write_header_to_logfile(std::ofstream* output_file, Configuration configuration)
{
    int amount = (int) configuration.parameters.size();
    *output_file << "Date, ";
    for (int i = 0; i < amount; ++i)
    {
            *output_file << configuration.parameters[i];
            if (i < amount - 1)
            {
                *output_file << ", ";
            }
    }
    *output_file << "\n";
}

// void make_sure_no_sleep_device(SerialPort* serialport, Configuration* configuration/*, int wait_time_between_commands*/)
// {
//     serialport->write(configuration->get_password());
//     serialport->write("login timeout 60\n");
//     serialport->write("lcd timeout 0\n");
// }

//get login and lcd timeout from a woken up and logged in device to int variables whose memory location is specified in arguments
//values are set to -1 if any read was unsuccessful
void get_timeouts(SerialPort* serialport, char* read_buf, size_t buff_size, int* login_timeout, int* lcd_timeout/*, int wait_time_between_commands*/)
{
    serialport->write("login timeout\n");
    serialport->read(read_buf);
    std::string log = get_value_of(read_buf, buff_size, "login", false);

    serialport->write("lcd timeout\n");
    serialport->read(read_buf);
    std::string lcd = get_value_of(read_buf, buff_size, "lcd", false);
    if (log == "" || lcd == "")
    {
        *login_timeout = -1;
        *lcd_timeout = -1;
        return;
    }
    *login_timeout = stoi(log);
    *lcd_timeout = stoi(lcd);
    DEBUG_LOG("Otrzymane limity czasu:\t\tLogin %d\t\tLcd %d\n", *login_timeout, *lcd_timeout);
    // std::cout << "LOGIN: " << *login_timeout << "\t\tLCD: " << *lcd_timeout << "\n";
}

//set login and lcd timeout in a woken up and logged in device to values specified in arguments
void set_timeouts(SerialPort* serialport, int login_timeout, int lcd_timeout/*, int wait_time_between_commands*/)
{
    DEBUG_LOG("Limity czasu do ustawienia:\tLogin %d \t\tLcd %d\n", login_timeout, lcd_timeout);
    // cerr << "Timeouts setting\tLOGIN: " << login_timeout << "\t\tLCD: " << lcd_timeout << "\n";
    serialport->write("login timeout " + std::to_string(login_timeout) + "\n");
    serialport->write("lcd timeout " + std::to_string(lcd_timeout) + "\n");
}

//get login and lcd timeouts from a woken up and logged in device to int variables and set them to specified values
void timeout_switcheroo(SerialPort* serialport, char* read_buf, size_t buff_size, int* old_login_timeout, int* old_lcd_timeout, int new_login_timeout, int new_lcd_timeout/*, int wait_time_between_commands*/)
{
    get_timeouts(serialport, read_buf, buff_size, old_login_timeout, old_lcd_timeout);
    if (*old_login_timeout != -1 && *old_lcd_timeout != -1)
    {
        set_timeouts(serialport, new_login_timeout, new_lcd_timeout);
    }
}

//changes movmean map elements to arithmetic means of all elements in measurement_values map, with the same key
void calculate_movmean(Configuration configuration, std::map<std::string, std::queue<std::string>> measurement_values, std::map<std::string, double> movmean)
{
    //iterate over all measurement_values map elements - queues of measurements of each parameter
    for (std::map<std::string, std::queue<std::string>>::iterator it = measurement_values.begin(); it != measurement_values.end(); ++it)
    {
        //calculate and display only if there are at least two readings of a volume parameter so a delta can be meaningful
        if (it->second.empty() || it->second.size() < 2 || (it->first != "Vm" && it->first != "Vc" && it->first != "Vb" && it->first != "Vr"))
        {
            continue;
        }
        std::string first = it->second.front();
        std::string last = it->second.back();
        DEBUG_LOG("Pierwszy pomiar: %s\tOstatni pomiar: %s\tRozmiar okna: %d\n", first.c_str(), last.c_str(), (int) it->second.size());
        movmean[it->first] = (double (std::stod(last) - std::stod(first))) / (double) (it->second.size() - 1);
        DEBUG_LOG("Wyliczona wartość średniej kroczącej dla parametru %s to: %f\n", it->first.c_str(), movmean[it->first]);
        printf("Średnia krocząca Δ%s:\t%0.6fm³\n", it->first.c_str(), movmean[it->first]);
        DEBUG_LOG("Średni przepływ:\t%0.6fm³/s\n", movmean[it->first] / (double) configuration.interval);
        #ifndef DEBUG
            std::ignore = configuration;
        #endif
    }
}

//  EXAMPLES of proper use:

//  Old version:
// ./pomiar_obj /dev/ttyUSB0 file_name.txt Vc Vb
// ./pomiar_obj COM3 my_log.txt 30 temperature Vc

//  New version:
// ./pomiar_obj -d /dev/ttyUSB0 -l file_name.txt -p Vc Vb
// ./pomiar_obj -d /dev/ttyACM0 -p temperature -i 30
// ./pomiar_obj -d /dev/ttyUSB0 -l my_log.txt -p Vc Vb temperature -i 5

// To compile on Windows:
// sudo apt install mingw-w64
// x86_64-w64-mingw32-g++ src/*.cpp -static -static-libgcc -static-libstdc++ -o out.exe
int main(int argc, char **argv)
{
    //change cmd encoding for proper locale display on windows
    #ifdef _WIN32
        SetConsoleOutputCP(CP_UTF8);
    #endif
    //store terminal options into config data
    Configuration* configuration = parse_terminal_arguments(argc, argv);
    if (configuration->if_terminate)
    {
        return 1;
    }
    //display full configuration data
    std::cout << *configuration;

    //open a port on a specified path
    SerialPort* serialport;
    #ifdef __linux__
        SerialPortLinux sp(configuration->device);
    #elif _WIN32
        SerialPortWin sp(configuration->device);
    #endif
    if (!sp.isOpen())
    {
        fprintf(stderr, "Nie udało się otworzyć portu\n");
        return 1;
    }
    serialport = &sp;

    //setup program termination handlers for manual termination handling and proper program behaviour
    #ifdef _WIN32
        SetConsoleCtrlHandler(CtrlHandler, TRUE);
    #elif __linux__
        struct sigaction sig_int_handler;
        sig_int_handler.sa_handler = signal_handler;
        sigemptyset(&sig_int_handler.sa_mask);
        sig_int_handler.sa_flags = 0;
        sigaction(SIGINT, &sig_int_handler, NULL);
    #endif
    //start waiting for simple button presses for program termination without displaying them/requiring Enter
    #ifdef __linux__
        enable_raw_mode();
    #endif

    char read_buf[BUFF_SIZE];
    size_t buff_size = BUFF_SIZE;
    const std::string msg_meter_show = { "\n\nmeter show\n" };
    // int wait_time_between_commands = 20;

    //write a data header to the log file
    std::ofstream output_file(configuration->log_file);
    memset(&read_buf, '\0', sizeof(read_buf));

    //start sending messages - at first, the device asks for a password for menu access
    serialport->write("\n");
    serialport->write(configuration->get_password());

    //turn off timeouts during runtime: read current timeouts from the device connected to serialport to restore them during program termination
    int read_login_timeout, read_lcd_timeout, tmp_lcd, tmp_log;
    read_login_timeout  = read_lcd_timeout = -1;
    tmp_lcd = tmp_log = 0;
    timeout_switcheroo(serialport, read_buf, buff_size, &read_login_timeout, &read_lcd_timeout, tmp_log, tmp_lcd);
    
    time_t read_time, current_time;
    log_streams s;
    s.firstLoop = true;

    //main loop: send requests, parse response for requested parameter values, append log file, wait until next interval, repeat
    while (true)
    {
        //get timestamp of the measurement, display it later to account for the need to resend the meter show message
        read_time = time(0);

        bool resend_meter_show = false;
        //send a request for readings to the device
        memset(&read_buf, '\0', sizeof(read_buf));
        serialport->write(msg_meter_show);

        //get response from the serial port to read_buf buffer
        serialport->read(read_buf);

        std::string buffer(read_buf);
        // wrong password response should happen only if the device just requested password, which should occur only if
        // the device has been woken up during program runtime AND has already been logged out due to its timeout
        // in that case: log in and turn off timeouting, set meter show to be repeated
        size_t found = buffer.find("Wrong password");
        if (found != std::string::npos)
        {
            DEBUG_LOG("Powtarzanie przesyłania hasła\n");
            // // serialport->write(configuration->get_password());
            // // make_sure_no_sleep_device(serialport, configuration, wait_time_between_commands);
            // // memset(&read_buf, '\0', sizeof(read_buf));
            serialport->write(configuration->get_password());
            resend_meter_show = true;

            // // timeout_switcheroo(serialport, read_buf, &read_login_timeout, &read_lcd_timeout, tmp_log, tmp_lcd);

            // // serialport->write(msg_meter_show);
            // // serialport->read(read_buf);
        }

        //if the device was woken up during runtime, regardless of whether user was logged in after wake up or not, especially lcd timeout
        //has to be set to 0 so that the device will never fall asleep during measurement; set meter show to be repeated
        //if device is still asleep, there will simply be a second attempt to get the readings and wait for readout interval will commence
        //as usual
        if (read_lcd_timeout == -1)
        {
            timeout_switcheroo(serialport, read_buf, buff_size, &read_login_timeout, &read_lcd_timeout, tmp_log, tmp_lcd);
            resend_meter_show = true;
        }

        //repeat meter show message due to first wakeup/error, get new more accurate timestamp
        if (resend_meter_show)
        {
            // memset(&read_buf, '\0', sizeof(read_buf));
            read_time = time(0);
            serialport->write(msg_meter_show);
            serialport->read(read_buf);
        }

        //actually display the timestamp to make sure the one shown is of the readout attempt
        std::string read_time_str = std::asctime(std::localtime(&read_time));
        read_time_str.pop_back();
        printf("\nCzas pomiaru: %s\n", read_time_str.c_str());

        // // serialport->write(configuration->get_password());

        //look through the buffer to get values of requested parameters to console + log file
        // get_readings(*configuration, &s, read_buf, &output_file, read_time_str);
        // //look through the buffer to get values of requested parameters to console + log file
        if (!get_readings(*configuration, &s, read_buf, buff_size, &output_file, read_time_str))
        {
            DEBUG_LOG("Odczyt się nie powiódł\n");
            DEBUG_LOG("\n================================ Bufor:\n");
            DEBUG_LOG("%s\n", read_buf);
            DEBUG_LOG("=======================================\n");
            // cerr << "\n================================ Bufor:\n";
            // cerr << read_buf << "\n";
            // cerr << "=======================================\n";
        // //     // make_sure_no_sleep_device(serialport, configuration, wait_time_between_commands);
        // //     // serialport->write(configuration->get_password());
        // //     // // memset(&read_buf, '\0', sizeof(read_buf));
        // //     // timeout_switcheroo(serialport, read_buf, &read_login_timeout, &read_lcd_timeout, tmp_log, tmp_lcd);

        // //     serialport->write(msg_meter_show);
        // //     serialport->read(read_buf);
        // //     get_readings(*configuration, &s, read_buf, &output_file, read_time_str);
        }
        else
        {
            std::map<std::string, double> movmean;
            calculate_movmean(*configuration, s.measurement_values, movmean);
        }

        //display the full message for debugging purposes
        // DEBUG_LOG("\nCzas pomiaru: %s\n", read_time_str.c_str());
        // DEBUG_LOG("\n================================ Bufor:\n");
        // DEBUG_LOG("%s\n", read_buf);
        // DEBUG_LOG("=======================================\n");

        //wait out time between intervals, react to any pressed button with program termination
        do
        {
            if (_kbhit())
            {
                #ifdef __linux__
                    std::cin.ignore();
                #elif _WIN32
                    _getch();
                #endif
                printf("\nTerminacja programu, wciśnięto przycisk\n");
                signal_received = 1;
            }
            if (signal_received)
            {
                #ifdef __linux__
                    disable_raw_mode();
                #endif
                if (s.firstLoop && output_file.is_open())
                {
                    write_header_to_logfile(&output_file, *configuration);
                    output_file << s.reading.str() << std::flush;
                }
                // restore timeouts from user settings
                set_timeouts(serialport, read_login_timeout, read_lcd_timeout);
                serialport->close();
                output_file.close();
                delete configuration;
                return 0;
            }
            current_time = time(0);
        }
        while (current_time - read_time < configuration->interval);
    }
    return 0;
}
