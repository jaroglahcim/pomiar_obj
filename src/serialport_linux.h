#ifdef __linux__
    #include "serialport.h"

    class SerialPortLinux : public SerialPort
    {
    public:
        SerialPortLinux(std::string fileName);
        ~SerialPortLinux();
        void open() override;
        void close() override;
        ssize_t read(char* buffer, int buff_size) override;
        ssize_t write(const std::string& message) override;
    private:
        int serial_port_;
        void configure() override;
    };
#endif
