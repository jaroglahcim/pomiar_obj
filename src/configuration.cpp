#include "configuration.h"
#include <fstream> //ifstream
#include <sstream> //stringstream
#include <iostream>

Configuration::Configuration(std::string device, time_t interval, unsigned int movmean_window, std::string log_file,
                        std::vector<std::string> parameters, std::string config_file, std::string msg_pass) :
                        device(device), interval(interval), movmean_window(movmean_window), log_file(log_file), parameters(parameters), config_file(config_file), msg_pass(msg_pass)

{
    // this->device = device;
    // this->interval = interval;
    // this->log_file = log_file;
    // this->parameters = parameters;
    // this->config_file = config_file;
    // this->msg_pass = msg_pass;
}

//helper function used in function for loading up configuration from a file
void Configuration::store_config_line(std::string key, std::string value)
{
    switch(pomiar_obj::hash(key.c_str()))
    {
        case pomiar_obj::hash("device"):
        {
            device = value;
            break;
        }
        case pomiar_obj::hash("interval"):
        {
            char *conversion_result;
            double d = strtod(value.c_str(), &conversion_result);
            if (*conversion_result == '\0')
            {
                interval = (time_t) d;
            }
            break;
        }
        case pomiar_obj::hash("logfile"): case pomiar_obj::hash("log"):
        {
            log_file = value;
            break;
        }
        case pomiar_obj::hash("parameters"):
        {
            parameters.clear();
            std::istringstream is_parameters(value);
            while(getline(is_parameters, key, ','))
            {
                parameters.push_back(key);
            }
            break;
        }
        case pomiar_obj::hash("password"):
        {
            msg_pass = value + "\n";
            break;
        }
        case pomiar_obj::hash("movmean"):
        {
            char *conversion_result;
            long i = strtol(value.c_str(), &conversion_result, 10);
            if (*conversion_result == '\0')
            {
                movmean_window = (int) i;
            }
            break;
        }
        default:
        {
            break;
        }
    }
}

void Configuration::get_configuration_from_file()
{
    std::string line;
    std::ifstream in(config_file);
    if(!in.fail())
    {
        while(getline(in, line))
        {
            std::istringstream is_line(line);
            std::string key;
            if(getline(is_line, key, '='))
            {
                std::string value;
                if(getline(is_line, value))
                store_config_line(key, value);
            }
        }
        in.close();
        std::cout << "Wczytano ustawienia z pliku konfiguracyjnego\n";
    }
    else
    {
        in.close();
        std::cerr << "Nie można otworzyć pliku konfiguracyjnego\n";
    }
}


std::string const Configuration::get_password() 
{
    return msg_pass;
}


std::ostream& operator<<(std::ostream &out, const Configuration& c)
{
    out << "\n\nUrządzenie: " + c.device + "\nPlik logów: " + c.log_file + "\nInterwał odczytów: " + std::to_string(c.interval) +
            "\nOkno średniej kroczącej: ";
    if (c.movmean_window >= 2)
    {
        out << std::to_string(c.movmean_window) + "\n";
    }
    else
    {
        out << "BRAK\n";
    }
    for(size_t i = 0; i < c.parameters.size(); i++)
    {
        out << "Parametr do odczytania: " + c.parameters[i] + "\n";
    }
    out << "Plik konfiguracyjny: ";
    if (c.config_file != "")
    {
        out << c.config_file + "\n";
    }
    else
    {
        out << "BRAK\n";
    }
    out << "Hasło: " << c.msg_pass << "\n";
    return out;
}

std::string Configuration::to_string()
{
    std::stringstream out;
    out << "\n\nUrządzenie: " + device + "\nPlik logów: " + log_file + "\nInterwał odczytów: " + std::to_string(interval) + "\nOkno średniej kroczącej: ";
    if (movmean_window >= 2)
    {
        out << std::to_string(movmean_window) + "\n";
    }
    else
    {
        out << "BRAK\n";
    }
    for(size_t i = 0; i < parameters.size(); i++)
    {
        out << "Parametr do odczytania: " + parameters[i] + "\n";
    }
    out << "Plik konfiguracyjny: " + config_file + "\n";
    return out.str();
}
