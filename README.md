## OPIS
Program pomiar_obj odczytuje z liczydła iSmart2 (w trybie tishell) przez głowicę optyczną parametry, np. "Vb", podany w trakcie uruchamiania. Odczyt odbywa się co podaną liczbę sekund (domyślnie 2s) i wynik odczytu, wraz z godziną, wypisywane są na ekranie konsoli i do pliku. Program działa w systemach Linux i Windows.

## WYMAGANIA DO KOMPILACJI
1. Linux:

- gcc 7.5.0 (działa na: 11.2.0)
- make 4.1 (działa na: 4.3)
- cmake min. 3.17 (działa na: 3.18.4, 3.20.3)

2. Linux dla Windowsa:

- mingw-w64 (działa na: 10-win32 20210610)<br>
`sudo apt install mingw-w64`

3. Windows:

- g++

## BUDOWANIE PROJEKTU
1. Z poziomu głównego katalogu projektu <...>/pomiar_obj/ w systemie Linux:  
`cd build`  
`cmake ..`  
`make`

2. Tworzenie pliku wykonywalnego dla systemu Windows na Linuksie:
`x86_64-w64-mingw32-g++ ../src/*.cpp -static -static-libgcc -static-libstdc++ -o pomiar_obj.exe`

3. Na systemie Windows, z użyciem samego tylko g++:
`cd build`  
`g++ ..\src\pomiar_obj.cpp ..\src\serialport_win.cpp ..\src\configuration.cpp -o pomiar_obj.exe -Wall -Wextra -Werror -pedantic -Wconversion -O3`

## URUCHAMIANIE
W poniższym przykładzie założono, że użytkownik znajduje się w katalogu /pomiar_obj/build/  
1. Podłączyć głowicę optyczną do liczydła.  
2. **Uruchomić liczydło jego lewym przyciskiem.**  
3. Uruchomić program w terminalu.

Najprostsze uruchomienie - tryb interaktywny, w którym użytkownik jest pytany o wartość różnych opcji wywołania programu. Enter powoduje przyjęcie wartości domyślnych, podawanych w nawiasie.
Aby uruchomić program w trybie interaktywnym, należy zwyczajnie go uruchomić bez podawania żadnej opcji (`./pomiar_obj`lub `./pomiar_obj.exe`, albo uruchomić plik wykonywalny exe zwyczajowym dwuklikiem).

Uruchomienie programu z ustawieniami zdefiniowanymi w pliku konfiguracyjnym wymaga wywołania programu z opcją -c, np. `./pomiar_obj.exe -c konfiguracja.txt`

Można też ustawić wartość różnych parametrów bezpośrednio w opcjach wywołania. Dokładny opis znajduje się we wbudowanej pomocy programu.

## Wbudowana pomoc:

Opis bardziej złożonej konfiguracji zapytań jest wbudowany w program wykonywalny i dostępny za pomocą wywołań:

- Opis krótki: `./pomiar_obj -h` albo `./pomiar_obj ?`

- Opis długi:  `./pomiar_obj --help`

Dostępne są opcje z linii wywołania i plik konfiguracyjny z ustawieniami. Szczegółowe informacje dostępne przy pomocy powyższych wywołań.